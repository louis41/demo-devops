const fullName = require('../name');

test('Returns Louis Concepcion if firstName and lastName are given', ()=> {
  expect(fullName('Louis', 'Concepcion')).toBe('Louis Concepcion');
});

test('Returns World if firstName or lastName is missing', ()=> {
  expect(fullName(undefined, undefined)).toBe('World');
});